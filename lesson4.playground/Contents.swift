// Условия

// if (codition1) {
//    do something
//} else if (condition2) {
//    do something
//} else {
//    do something
//}


/*
 switch var {
    case value1:
        do something
     case value2:
         do something
    default:
        do something
 }
*/

// Если нужно пойти дальше по цепочке кейсов, добавить fallthrough после кейса

var a = 1

switch a {
case 1: print("ok")
    fallthrough
case 2: print(">")
case 0: print("<")
default: print("no value")
}

let b = 10

switch b{
case 0...10: print("too young")
case 11..<20: print("too old")
default: print("no age")
}
