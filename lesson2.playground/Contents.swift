// Типы данных

var number: Int = 10
var a = 20
var b = -20
var c = 10.22

let sum = Double(a) + c
let sumString = String(a) + " Hello"

Int.min
Int.max

UInt.min
UInt.max

Int8.min
Int8.max

UInt8.min
UInt8.max

Int16.min
Int16.max

UInt16.min
UInt16.max

Int32.min
Int32.max

UInt32.min
UInt32.max

Int64.min
Int64.max

UInt64.min
UInt64.max



var x = 10.32

var float: Float = 1.42123123312312323
var double: Double = 1.42123123312312323


var string = "Hello world"

var t = "hello"
var w = "world"
var concat = t + " " + w

print("\(t) \(w)!")


var bool = false

if (bool) {
    print("good")
} else {
    print("bad")
}
